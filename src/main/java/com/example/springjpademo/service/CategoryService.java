package com.example.springjpademo.service;

import com.example.springjpademo.model.CategoryItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface CategoryService {

    Page<CategoryItem> findAll(Pageable pageable);

    CategoryItem findOne(Long id);

    CategoryItem add(CategoryItem item);

    CategoryItem update(Long id, CategoryItem item);

    boolean delete(Long id);

}
