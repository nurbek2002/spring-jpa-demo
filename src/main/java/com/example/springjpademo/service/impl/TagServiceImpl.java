package com.example.springjpademo.service.impl;

import com.example.springjpademo.entity.Tag;
import com.example.springjpademo.model.TagItem;
import com.example.springjpademo.repository.TagRepository;
import com.example.springjpademo.service.TagService;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
class TagServiceImp implements TagService {

    private final TagRepository tagRepository;

    public TagServiceImp(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<TagItem> search(String keyword) {
        List<Tag> tags = tagRepository.findTop10ByTitleContainingIgnoreCase(keyword);
        if (tags == null) return Collections.emptyList();

        return tags
                .stream()
                .map(TagItem::fromTag)
                .collect(Collectors.toList());
    }
}
