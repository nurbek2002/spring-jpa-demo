package com.example.springjpademo.service.impl;

import com.example.springjpademo.entity.Category;
import com.example.springjpademo.model.CategoryItem;
import com.example.springjpademo.repository.CategoryRepository;
import com.example.springjpademo.service.CategoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public Page<CategoryItem> findAll(Pageable pageable) {
        return repository.findAll(pageable).map(CategoryItem::fromCategory);
    }

    @Override
    public CategoryItem findOne(Long id) {
        return CategoryItem.fromCategory(repository.getById(id));
    }

    @Override
    public CategoryItem add(CategoryItem item) {
        Category category = new Category();
        category.setName(item.getName());
        return CategoryItem.fromCategory(repository.save(category));
    }

    @Override
    public CategoryItem update(Long id, CategoryItem item) {
        Category category = repository.getById(id);
        category.setName(item.getName());
        return CategoryItem.fromCategory(repository.save(category));
    }

    @Override
    public boolean delete(Long id) {
        Optional<Category> optional = repository.findById(id);
        if (optional.isEmpty()) {
            return false;
        }
        repository.delete(optional.get());
        return true;
    }
}
