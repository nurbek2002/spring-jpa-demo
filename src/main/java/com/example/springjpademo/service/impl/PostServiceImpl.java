package com.example.springjpademo.service.impl;

import com.example.springjpademo.entity.Post;
import com.example.springjpademo.entity.Tag;
import com.example.springjpademo.model.PostForm;
import com.example.springjpademo.model.PostItem;
import com.example.springjpademo.model.PostListItem;
import com.example.springjpademo.model.TagItem;
import com.example.springjpademo.repository.CategoryRepository;
import com.example.springjpademo.repository.PostRepository;
import com.example.springjpademo.repository.TagRepository;
import com.example.springjpademo.service.PostService;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;
    private final CategoryRepository categoryRepository;
    private final TagRepository tagRepository;
    private final EntityManager entityManager;

    public PostServiceImpl(PostRepository postRepository,
                           CategoryRepository categoryRepository,
                           TagRepository tagRepository,
                           EntityManager entityManager) {
        this.postRepository = postRepository;
        this.categoryRepository = categoryRepository;
        this.tagRepository = tagRepository;
        this.entityManager = entityManager;
    }

    @Override
    public List<PostListItem> list() {
        return postRepository.findAll().stream()
                .map(PostListItem::fromPost).collect(Collectors.toList());
    }

    @Override
    public PostItem get(Long id) {
        return PostItem.fromPost(postRepository.getById(id));
    }

    @Override
    public PostItem add(PostForm postForm) {
        Post post = new Post();
        post.setTitle(postForm.getTitle());
        post.setContent(postForm.getContent());
        post.setCategory(categoryRepository.getById(postForm.getCategoryId()));
        post.setTags(fromTagItemSet(postForm.getTags()));

        return PostItem.fromPost(postRepository.save(post));
    }

    @Override
    public PostItem update(Long id, PostForm postForm) {
        Post post = postRepository.getById(id);
        post.setTitle(postForm.getTitle());
        post.setContent(postForm.getContent());
        post.setTags(fromTagItemSet(postForm.getTags()));
        return PostItem.fromPost(postRepository.save(post));
    }

    @Override
    public boolean delete(Long id) {
        postRepository.deleteById(id);
        return true;
    }

    private Set<Tag> fromTagItemSet(Set<TagItem> tagItems) {
        return tagItems
                .stream()
                .map(tagItem -> {
                    if (tagItem.getId() != null) {
                        return entityManager.getReference(Tag.class, tagItem.getId());
                    } else {
                        Tag tag = new Tag();
                        tag.setTitle(tagItem.getTitle());
                        return tagRepository.save(tag);
                    }
                }).collect(Collectors.toSet());
    }
}
