package com.example.springjpademo.service;

import com.example.springjpademo.model.PostForm;
import com.example.springjpademo.model.PostItem;
import com.example.springjpademo.model.PostListItem;

import java.util.List;

public interface PostService {

    List<PostListItem> list();

    PostItem get(Long id);

    PostItem add(PostForm postForm);

    PostItem update(Long id, PostForm postForm);

    boolean delete(Long id);

}
