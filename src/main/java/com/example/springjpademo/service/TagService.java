package com.example.springjpademo.service;

import com.example.springjpademo.model.TagItem;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TagService {

    List<TagItem> search(String keyword);

}
