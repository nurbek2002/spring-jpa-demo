package com.example.springjpademo.controller;

import com.example.springjpademo.model.CategoryItem;
import com.example.springjpademo.service.CategoryService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/category")
public class CategoryController {

    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping
    public ResponseEntity<?> findAll(Pageable pageable) {
        return ResponseEntity.ok(categoryService.findAll(pageable));
    }

    @GetMapping("{id}")
    public ResponseEntity<?> findOne(@PathVariable Long id) {
        return ResponseEntity.ok(categoryService.findOne(id));
    }

    @PostMapping
    public ResponseEntity<?> add(@RequestBody CategoryItem item) {
        return ResponseEntity.ok(categoryService.add(item));
    }

    @PutMapping({"{id}"})
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody CategoryItem item) {
        return ResponseEntity.ok(categoryService.update(id, item));
    }
}
