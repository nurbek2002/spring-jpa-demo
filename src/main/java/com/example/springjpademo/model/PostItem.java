package com.example.springjpademo.model;

import com.example.springjpademo.entity.Post;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostItem {

    private Long id;

    private String title;

    private String content;

    private Long date;

    public static PostItem fromPost(Post post) {
        PostItem item = new PostItem();
        item.setId(post.getId());
        item.setTitle(post.getTitle());
        item.setContent(post.getContent());
        item.setDate(post.getCreatedDate().getTime());
        return item;
    }


}
