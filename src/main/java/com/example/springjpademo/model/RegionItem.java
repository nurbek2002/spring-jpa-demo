package com.example.springjpademo.model;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class RegionItem {

    private long id;

    private String name;

    Set<DistrictItem> districts;

    @Getter
    @Setter
    public static class DistrictItem {

        private Long id;

        private String name;

    }

}
