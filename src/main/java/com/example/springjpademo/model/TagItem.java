package com.example.springjpademo.model;

import com.example.springjpademo.entity.Tag;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TagItem {

    private Long id;

    private String title;

    public static TagItem fromTag(Tag tag) {
        TagItem item = new TagItem();
        item.setId(tag.getId());
        item.setTitle(tag.getTitle());
        return item;
    }

}
