package com.example.springjpademo.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;

import java.util.Set;

@Getter
@Setter
public class PostForm {

    @NonNull
    private String title;

    private String content;

    private Long categoryId;

    private Set<TagItem> tags;

}
