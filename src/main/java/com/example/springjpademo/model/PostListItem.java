package com.example.springjpademo.model;

import com.example.springjpademo.entity.Post;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostListItem {

    private Long id;

    private String title;

    private Long date;

    private CategoryItem category;

    public static PostListItem fromPost(Post post) {
        PostListItem item = new PostListItem();
        item.setId(post.getId());
        item.setTitle(post.getTitle());
        item.setDate(post.getCreatedDate().getTime());
        item.setCategory(CategoryItem.fromCategory(post.getCategory()));
        return item;
    }


}
